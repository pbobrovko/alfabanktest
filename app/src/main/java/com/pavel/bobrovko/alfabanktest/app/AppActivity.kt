package com.pavel.bobrovko.alfabanktest.app

import android.os.Bundle
import arellomobile.moxy.MvpAppCompatActivity
import com.pavel.bobrovko.alfabanktest.R
import com.pavel.bobrovko.alfabanktest.core.FlowFragment
import com.pavel.bobrovko.alfabanktest.core.Screens
import com.pavel.bobrovko.alfabanktest.di.DI
import com.pavel.bobrovko.alfabanktest.extension.setLaunchScreen
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import toothpick.Scope
import toothpick.Toothpick
import javax.inject.Inject

class AppActivity : MvpAppCompatActivity() {

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    private val navigator: Navigator = SupportAppNavigator(this, supportFragmentManager, R.id.appContainer_fl)

    private val currentFlowFragment: FlowFragment?
        get() = supportFragmentManager.findFragmentById(R.id.appContainer_fl) as? FlowFragment

    private lateinit var scope: Scope

    override fun onCreate(savedInstanceState: Bundle?) {
        scope = Toothpick.openScope(DI.APP_SCOPE)
        Toothpick.inject(this, scope)
        setTheme(R.style.AppTheme)

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_app)

        if (savedInstanceState == null) {
            navigator.setLaunchScreen(Screens.MainFlow)
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()

        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()

        super.onPause()
    }

    override fun onBackPressed() {
        currentFlowFragment?.onBackPressed() ?: super.onBackPressed()
    }
}
