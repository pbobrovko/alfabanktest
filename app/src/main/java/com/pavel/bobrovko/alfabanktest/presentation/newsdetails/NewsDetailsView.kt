package com.pavel.bobrovko.alfabanktest.presentation.newsdetails

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.pavel.bobrovko.alfabanktest.model.db.News

@StateStrategyType(AddToEndSingleStrategy::class)
interface NewsDetailsView : MvpView {

    fun setNewsList(newsList: List<News>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    fun setProgressVisible()

    fun setProgressGone()

    fun setInitialPosition(newsPosition: Int)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun createPagesDir()
}