package com.pavel.bobrovko.alfabanktest.model.interactor

import com.pavel.bobrovko.alfabanktest.model.db.News
import io.reactivex.Completable
import io.reactivex.Flowable

interface NewsInteractor {

    fun loadInitialNewsList(): Completable

    fun getNewsList(isHotNews: Boolean): Flowable<List<News>>

    fun reloadNewsList(): Completable

    fun updateNewsLocalPath(localPath: String, guid: String): Completable
}