package com.pavel.bobrovko.alfabanktest.di.module

import android.content.Context
import com.pavel.bobrovko.alfabanktest.App
import com.pavel.bobrovko.alfabanktest.model.system.AppSchedulers
import com.pavel.bobrovko.alfabanktest.model.system.SchedulersProvider
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class AppModule(
    app: App
) : Module() {

    init {
        bind(Context::class.java).toInstance(app)
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())

        // Navigation
        val cicerone = Cicerone.create()
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)
    }
}