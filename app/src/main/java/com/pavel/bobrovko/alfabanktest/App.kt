package com.pavel.bobrovko.alfabanktest

import android.app.Application
import androidx.work.WorkManager
import com.pavel.bobrovko.alfabanktest.di.DI
import com.pavel.bobrovko.alfabanktest.di.module.AppModule
import com.pavel.bobrovko.alfabanktest.di.module.ServerModule
import com.pavel.bobrovko.alfabanktest.model.workmanager.NewsUpdateWorkerFactory
import toothpick.Toothpick
import toothpick.configuration.Configuration

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        initDependencies()
    }

    private fun initDependencies() {
        val configuration = if (BuildConfig.DEBUG) {
            Configuration.forDevelopment().preventMultipleRootScopes()
        } else {
            Configuration.forProduction()
        }
        Toothpick.setConfiguration(configuration)

        val appScope = Toothpick.openScope(DI.APP_SCOPE)
        appScope.installModules(AppModule(this))
        val serverScope = Toothpick.openScopes(DI.APP_SCOPE, DI.SERVER_SCOPE)
        serverScope.installModules(ServerModule())

        val workerConfiguration = androidx.work.Configuration.Builder()
            .setWorkerFactory(serverScope.getInstance(NewsUpdateWorkerFactory::class.java))
            .build()
        WorkManager
            .initialize(this, workerConfiguration)
    }
}