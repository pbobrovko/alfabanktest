package com.pavel.bobrovko.alfabanktest.ui.newsdetails.viewPager

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.fragment.app.Fragment
import com.pavel.bobrovko.alfabanktest.R
import com.pavel.bobrovko.alfabanktest.extension.getPagesPath
import com.pavel.bobrovko.alfabanktest.extension.isInternetAvailable
import com.pavel.bobrovko.alfabanktest.model.db.News
import com.pavel.bobrovko.alfabanktest.ui.newsdetails.OnNewsArchiveListener
import kotlinx.android.synthetic.main.fragment_view_pager_news.*
import org.threeten.bp.Instant

class NewsViewPagerFragment : Fragment() {

    private lateinit var news: News
    private lateinit var onNewPageSavedListener: OnNewsArchiveListener

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (parentFragment is OnNewsArchiveListener) {
            onNewPageSavedListener = parentFragment as OnNewsArchiveListener
        } else {
            throw IllegalArgumentException("${javaClass.canonicalName} must be implement OnNewPageSavedListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        news = when {
            savedInstanceState != null -> savedInstanceState.getParcelable(EXTRAS_WEB_VIEW_NEWS)
            arguments != null -> arguments!!.getParcelable(EXTRAS_WEB_VIEW_NEWS)
            else -> throw IllegalArgumentException("${this.javaClass.canonicalName} need News object as argument")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_view_pager_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(newsWebView.settings) {
            loadWithOverviewMode = true
            useWideViewPort = true
            setSupportZoom(true)
            builtInZoomControls = true
            displayZoomControls = false
            cacheMode = WebSettings.LOAD_NO_CACHE
        }

        with(newsWebView) {
            scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
            isScrollbarFadingEnabled = false
            webViewClient = object : WebViewClient() {

                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    showContent(true)
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    newsWebViewProgress.visibility = View.GONE

                }

                override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                    super.onReceivedError(view, request, error)

                    showContent(false)
                }
            }

            if (!news.localPath.isNullOrEmpty()) {
                viewPagerAddToFavorites.visibility = View.GONE
            } else {
                viewPagerAddToFavorites.visibility = View.VISIBLE
            }

            when {
                requireContext().isInternetAvailable() -> loadUrl(news.link)
                !news.localPath.isNullOrEmpty() -> loadUrl("file://${news.localPath}")
                else -> showContent(false)
            }
        }

        viewPagerAddToFavorites.setOnClickListener { saveCurrentPage() }
    }

    private fun showContent(show: Boolean) {
        if (show) {
            newsWebViewProgress.visibility = View.VISIBLE
            newsWebView.visibility = View.VISIBLE
            viewPagerPageErrorText.visibility = View.GONE
        } else {
            newsWebViewProgress.visibility = View.GONE
            newsWebView.visibility = View.GONE
            viewPagerPageErrorText.visibility = View.VISIBLE
            viewPagerAddToFavorites.visibility = View.GONE
        }
    }

    private fun saveCurrentPage() {
        val timeStamp = news.pubDate?.toInstant()?.epochSecond ?: Instant.now().epochSecond
        val fileName = "$timeStamp.mht"
        val filePath = "${requireContext().getPagesPath()}$fileName"

        newsWebView.saveWebArchive(filePath)
        viewPagerAddToFavorites.visibility = View.INVISIBLE
        onNewPageSavedListener.onNewsArchived(filePath, news.guid)
        news = news.copy(localPath = filePath)
    }

    override fun onResume() {
        super.onResume()

        newsWebView.onResume()
    }

    override fun onPause() {
        super.onPause()

        newsWebView.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(EXTRAS_WEB_VIEW_NEWS, news)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        newsWebView.destroy()
    }

    companion object {
        private const val EXTRAS_WEB_VIEW_NEWS = "extrasWebViewNews"

        fun newInstance(news: News) = NewsViewPagerFragment().apply {
            arguments = Bundle().apply {
                putParcelable(EXTRAS_WEB_VIEW_NEWS, news)
            }
        }
    }
}