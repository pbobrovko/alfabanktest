package com.pavel.bobrovko.alfabanktest.ui.newslist.viewpager

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.snackbar.Snackbar
import com.pavel.bobrovko.alfabanktest.R
import com.pavel.bobrovko.alfabanktest.core.BaseFragment
import com.pavel.bobrovko.alfabanktest.core.wrappers.PrimitiveWrapper
import com.pavel.bobrovko.alfabanktest.extension.dpToPx
import com.pavel.bobrovko.alfabanktest.model.db.News
import com.pavel.bobrovko.alfabanktest.presentation.newslist.NewsListPresenter
import com.pavel.bobrovko.alfabanktest.presentation.newslist.NewsListView
import com.pavel.bobrovko.alfabanktest.ui.newslist.adapter.ItemDecorator
import com.pavel.bobrovko.alfabanktest.ui.newslist.adapter.NewsListAdapter
import kotlinx.android.synthetic.main.fragment_news_list.*
import toothpick.Scope
import toothpick.config.Module

class NewsListFragment : BaseFragment(), NewsListView {

    override val layoutRes = R.layout.fragment_news_list

    @InjectPresenter
    lateinit var presenter: NewsListPresenter

    @ProvidePresenter
    fun providePresenter(): NewsListPresenter = scope.getInstance(NewsListPresenter::class.java)

    private lateinit var newsAdapter: NewsListAdapter

    private var snackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        if (arguments == null) {
            throw IllegalArgumentException("${javaClass.canonicalName} need arguments")
        }
        super.onCreate(savedInstanceState)
    }

    override fun installScopeModules(scope: Scope) {
        scope.installModules(object : Module() {
            init {
                bind(PrimitiveWrapper::class.java)
                    .toInstance(PrimitiveWrapper(arguments!!.getBoolean(INITIAL_LIST_TYPE)))
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        newsAdapter = NewsListAdapter { presenter.onNewsClicked(it) }
        newsListView.layoutManager = LinearLayoutManager(requireContext())
        newsListView.addItemDecoration(ItemDecorator(requireContext().dpToPx(5f)))
        newsListView.adapter = newsAdapter

        swipeRefresh.setOnRefreshListener { presenter.refreshNewsList() }
    }

    override fun initEmptyView(message: String) {
        noNewsListText.text = message
    }

    override fun disableRefresh() {
        swipeRefresh.isEnabled = false
    }

    override fun setNewsList(newsList: List<News>) {
        newsAdapter.setItems(newsList)
    }

    override fun showEmptyView(show: Boolean) {
        if (show) {
            noNewsListText.visibility = View.VISIBLE
            newsListView.visibility = View.GONE
        } else {
            newsListView.visibility = View.VISIBLE
            noNewsListText.visibility = View.GONE
        }
    }

    override fun setRefreshFinished() {
        swipeRefresh.isRefreshing = false
    }

    override fun showMessage(message: String) {
        snackbar?.dismiss()
        snackbar = Snackbar.make(newsListLayout, message, Snackbar.LENGTH_LONG)
        snackbar?.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    companion object {
        private const val INITIAL_LIST_TYPE = "initialListType"

        fun newInstance(isHotNews: Boolean) =
            NewsListFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(INITIAL_LIST_TYPE, isHotNews)
                }
            }
    }
}