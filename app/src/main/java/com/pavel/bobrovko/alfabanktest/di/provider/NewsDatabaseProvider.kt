package com.pavel.bobrovko.alfabanktest.di.provider

import android.content.Context
import androidx.room.Room
import com.pavel.bobrovko.alfabanktest.model.db.NewsDao
import com.pavel.bobrovko.alfabanktest.model.db.NewsDatabase
import javax.inject.Inject
import javax.inject.Provider

class NewsDatabaseProvider @Inject constructor(
    private val context: Context
) : Provider<NewsDao> {

    companion object {
        private const val DB_NAME = "AlfaBankTestDB"
    }

    override fun get(): NewsDao {
        return Room
            .databaseBuilder(
                context,
                NewsDatabase::class.java, DB_NAME
            )
            .fallbackToDestructiveMigration()
            .build()
            .getNewsDatabase()
    }
}