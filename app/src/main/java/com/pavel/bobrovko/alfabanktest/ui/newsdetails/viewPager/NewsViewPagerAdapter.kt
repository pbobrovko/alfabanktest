package com.pavel.bobrovko.alfabanktest.ui.newsdetails.viewPager

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.pavel.bobrovko.alfabanktest.model.db.News


class NewsViewPagerAdapter(
    fragmentManager: FragmentManager
) : FragmentStatePagerAdapter(fragmentManager) {

    private val items = mutableListOf<News>()

    override fun getItem(position: Int) = NewsViewPagerFragment.newInstance(items[position])

    override fun getCount() = items.size

    fun setItems(items: List<News>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}