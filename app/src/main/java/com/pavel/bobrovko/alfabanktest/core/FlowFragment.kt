package com.pavel.bobrovko.alfabanktest.core

import androidx.annotation.CallSuper
import com.pavel.bobrovko.alfabanktest.di.DI
import com.pavel.bobrovko.alfabanktest.di.module.FlowNavigationModule
import com.pavel.bobrovko.alfabanktest.R
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import toothpick.Scope
import javax.inject.Inject

abstract class FlowFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_flow

    override val parentFragmentScopeName = DI.SERVER_SCOPE

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var router: Router

    protected val navigator by lazy { SupportAppNavigator(activity, childFragmentManager, R.id.flowContainer_fl) }

    private val currentFragment
        get() = childFragmentManager.findFragmentById(R.id.flowContainer_fl) as? BaseFragment

    override fun onResume() {
        super.onResume()

        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()

        super.onPause()
    }

    @CallSuper
    override fun installScopeModules(scope: Scope) {
        scope.installModules(FlowNavigationModule(scope.getInstance(Router::class.java)))
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }
}