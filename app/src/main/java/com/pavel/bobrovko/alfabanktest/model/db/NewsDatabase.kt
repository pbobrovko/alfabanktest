package com.pavel.bobrovko.alfabanktest.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [News::class], version = 1)
@TypeConverters(value = [ZonedDateTimeConverter::class])
abstract class NewsDatabase : RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "news_database"
    }

    abstract fun getNewsDatabase(): NewsDao
}