package com.pavel.bobrovko.alfabanktest.ui.newslist

import android.os.Bundle
import android.view.View
import com.pavel.bobrovko.alfabanktest.R
import com.pavel.bobrovko.alfabanktest.core.BaseFragment
import com.pavel.bobrovko.alfabanktest.ui.newslist.viewpager.NewsListViewPagerAdapter
import kotlinx.android.synthetic.main.fragmet_container_news_list.*
import toothpick.Scope

class NewsListContainerFragment : BaseFragment() {

    override val layoutRes = R.layout.fragmet_container_news_list

    override fun installScopeModules(scope: Scope) {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pagerAdapter = NewsListViewPagerAdapter(childFragmentManager, requireContext())
        newsListViewPager.adapter = pagerAdapter

        newsTabLayout.setupWithViewPager(newsListViewPager)
    }
}