package com.pavel.bobrovko.alfabanktest.ui.newsdetails

interface OnNewsArchiveListener {

    fun onNewsArchived(localPath: String, guid: String)
}