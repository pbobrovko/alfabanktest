package com.pavel.bobrovko.alfabanktest.model.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(newsList: List<News>): Completable

    @Query("SELECT * FROM ${NewsDatabase.DATABASE_NAME}")
    fun getHot(): Flowable<List<News>>

    @Query("UPDATE ${NewsDatabase.DATABASE_NAME} SET localPath = :localPath WHERE guid = :guid")
    fun updateLocalPath(localPath: String, guid: String): Completable

    @Query("SELECT * FROM ${NewsDatabase.DATABASE_NAME} WHERE localPath != ''")
    fun getFavorites(): Flowable<List<News>>
}