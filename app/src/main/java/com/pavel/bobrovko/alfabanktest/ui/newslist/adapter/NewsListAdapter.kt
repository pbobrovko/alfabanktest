package com.pavel.bobrovko.alfabanktest.ui.newslist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pavel.bobrovko.alfabanktest.R
import com.pavel.bobrovko.alfabanktest.model.db.News

class NewsListAdapter(
    private var onItemClickListener: ((String) -> Unit)
) : RecyclerView.Adapter<NewsListViewHolder>() {

    private val items = mutableListOf<News>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        NewsListViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_news_list, parent, false)
        )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: NewsListViewHolder, position: Int) {
        holder.bindView(items[position])
        holder.itemView.setOnClickListener {
            val holderPosition = holder.adapterPosition
            if (holderPosition > RecyclerView.NO_POSITION) {
                onItemClickListener.invoke(items[holderPosition].guid)
            }
        }
    }

    fun setItems(items: List<News>) {
        val oldItems = ArrayList(this.items)
        this.items.clear()
        this.items.addAll(items)

        val diffCallback = NewsListDiffCallback(oldItems, this.items)
        DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
    }
}