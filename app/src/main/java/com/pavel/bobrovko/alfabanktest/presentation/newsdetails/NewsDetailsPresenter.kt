package com.pavel.bobrovko.alfabanktest.presentation.newsdetails

import com.arellomobile.mvp.InjectViewState
import com.pavel.bobrovko.alfabanktest.core.BasePresenter
import com.pavel.bobrovko.alfabanktest.core.ErrorHandler
import com.pavel.bobrovko.alfabanktest.core.FlowRouter
import com.pavel.bobrovko.alfabanktest.core.Screens
import com.pavel.bobrovko.alfabanktest.core.wrappers.PrimitiveWrapper
import com.pavel.bobrovko.alfabanktest.model.interactor.NewsInteractor
import javax.inject.Inject

@InjectViewState
class NewsDetailsPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val newsInteractor: NewsInteractor,
    private val errorHandler: ErrorHandler,
    private val initialPageGuid: String,
    private val isHotNews: PrimitiveWrapper<Boolean>
) : BasePresenter<NewsDetailsView>() {

    private var isInitialEmit = true

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.createPagesDir()
        newsInteractor
            .getNewsList(isHotNews.value)
            .doOnSubscribe { viewState.setProgressVisible() }
            .doOnEach { viewState.setProgressGone() }
            .subscribe(
                {
                    viewState.setNewsList(it)
                    if (isInitialEmit) {
                        val initialPosition = it.indexOfFirst { it.guid == initialPageGuid }
                        viewState.setInitialPosition(if (initialPosition >= -1) initialPosition else 0)
                        isInitialEmit = false
                    }
                },
                { errorHandler.handleError(it) { message -> viewState.showMessage(message) } }
            )
            .connect()
    }

    fun onNewsArchived(localPath: String, guid: String) {
        newsInteractor.updateNewsLocalPath(localPath, guid)
            .subscribe(
                {},
                { errorHandler.handleError(it) { message -> viewState.showMessage(message) } }
            )
            .connect()
    }

    fun onBackPressed() {
        flowRouter.backTo(Screens.NewsListContainerScreen)
    }
}