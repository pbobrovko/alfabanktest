package com.pavel.bobrovko.alfabanktest.presentation.splash

import com.arellomobile.mvp.InjectViewState
import com.pavel.bobrovko.alfabanktest.core.BasePresenter
import com.pavel.bobrovko.alfabanktest.core.FlowRouter
import com.pavel.bobrovko.alfabanktest.core.Screens
import com.pavel.bobrovko.alfabanktest.model.interactor.NewsInteractorImpl
import javax.inject.Inject

@InjectViewState
class SplashPresenter @Inject constructor(
    private val newsInteractor: NewsInteractorImpl,
    private val flowRouter: FlowRouter
) : BasePresenter<SplashView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        newsInteractor
            .loadInitialNewsList()
            .subscribe(
                { handleLoadInitialNewsListResponse() },
                { handleLoadInitialNewsListResponse() }
            )
            .connect()
    }

    private fun handleLoadInitialNewsListResponse() {
        viewState.startUpdateWork()
        flowRouter.replaceScreen(Screens.NewsListContainerScreen)
    }
}