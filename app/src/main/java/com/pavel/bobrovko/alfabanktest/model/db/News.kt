package com.pavel.bobrovko.alfabanktest.model.db

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.ZonedDateTime

@Parcelize
@Entity(tableName = NewsDatabase.DATABASE_NAME)
data class News(
    val title: String,
    val link: String,
    val description: String,
    val pubDate: ZonedDateTime?,
    @PrimaryKey val guid: String,
    val localPath: String?
) : Parcelable