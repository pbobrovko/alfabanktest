package com.pavel.bobrovko.alfabanktest.di.module

import com.pavel.bobrovko.alfabanktest.core.ErrorHandler
import com.pavel.bobrovko.alfabanktest.di.provider.NewsDatabaseProvider
import com.pavel.bobrovko.alfabanktest.di.qualifier.RssServerUrl
import com.pavel.bobrovko.alfabanktest.model.db.NewsDao
import com.pavel.bobrovko.alfabanktest.model.interactor.NewsInteractor
import com.pavel.bobrovko.alfabanktest.model.interactor.NewsInteractorImpl
import com.pavel.bobrovko.alfabanktest.model.repository.NewsRepository
import com.pavel.bobrovko.alfabanktest.model.repository.NewsRepositoryImpl
import com.pavel.bobrovko.alfabanktest.model.system.resource.ResourceManager
import com.pavel.bobrovko.alfabanktest.model.system.resource.ResourceManagerImpl
import com.pavel.bobrovko.alfabanktest.model.workmanager.NewsUpdateWorkerFactory
import com.prof.rssparser.Parser
import toothpick.config.Module

class ServerModule : Module() {

    init {
        bind(Parser::class.java).toInstance(Parser())
        bind(NewsDao::class.java).toProvider(NewsDatabaseProvider::class.java).singletonInScope()
        bind(String::class.java).withName(RssServerUrl::class.java)
            .toInstance("https://alfabank.ru/_/rss/_rss.html?subtype=1&category=2&city=21")
        bind(ResourceManager::class.java).to(ResourceManagerImpl::class.java).singletonInScope()
        bind(ErrorHandler::class.java).to(ErrorHandler::class.java).singletonInScope()

        bind(NewsRepository::class.java).to(NewsRepositoryImpl::class.java).singletonInScope()
        bind(NewsInteractor::class.java).to(NewsInteractorImpl::class.java).singletonInScope()
        bind(NewsUpdateWorkerFactory::class.java).to(NewsUpdateWorkerFactory::class.java).singletonInScope()
    }
}