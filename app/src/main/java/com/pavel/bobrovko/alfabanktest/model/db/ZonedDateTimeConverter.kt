package com.pavel.bobrovko.alfabanktest.model.db

import androidx.room.TypeConverter
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter


class ZonedDateTimeConverter {

    companion object {
        @TypeConverter
        @JvmStatic
        fun toDate(date: String?): ZonedDateTime? {
            return if (date == null) null else ZonedDateTime.parse(date, DateTimeFormatter.RFC_1123_DATE_TIME)
        }

        @TypeConverter
        @JvmStatic
        fun fromDate(date: ZonedDateTime?): String? {
            return if (date == null) null else date.format(DateTimeFormatter.RFC_1123_DATE_TIME)
        }
    }
}