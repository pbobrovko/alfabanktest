package com.pavel.bobrovko.alfabanktest.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class RssServerUrl