package com.pavel.bobrovko.alfabanktest.presentation.splash

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface SplashView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun startUpdateWork()
}