package com.pavel.bobrovko.alfabanktest.ui.main

import android.os.Bundle
import com.pavel.bobrovko.alfabanktest.core.FlowFragment
import com.pavel.bobrovko.alfabanktest.core.Screens
import com.pavel.bobrovko.alfabanktest.extension.setLaunchScreen

class MainFlowFragment : FlowFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (childFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(Screens.SplashScreen)
        }
    }
}