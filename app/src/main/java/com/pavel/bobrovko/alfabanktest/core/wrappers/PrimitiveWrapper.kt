package com.pavel.bobrovko.alfabanktest.core.wrappers

import java.io.Serializable

data class PrimitiveWrapper<out T>(val value: T) : Serializable