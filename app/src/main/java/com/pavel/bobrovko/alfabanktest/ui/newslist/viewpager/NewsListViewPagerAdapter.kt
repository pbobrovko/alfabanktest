package com.pavel.bobrovko.alfabanktest.ui.newslist.viewpager

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.pavel.bobrovko.alfabanktest.R

class NewsListViewPagerAdapter(
    fragmentManager: FragmentManager,
    private val context: Context
) : FragmentStatePagerAdapter(fragmentManager) {

    private val items = mutableListOf(
        NewsListFragment.newInstance(true),
        NewsListFragment.newInstance(false)
    )

    override fun getItem(position: Int) = items[position]

    override fun getCount() = items.size

    override fun getPageTitle(position: Int) = when (position) {
        0 -> context.getString(R.string.tab_item_text_hot)
        1 -> context.getString(R.string.tab_item_text_favorites)
        else -> null
    }
}