package com.pavel.bobrovko.alfabanktest.model.repository

import com.pavel.bobrovko.alfabanktest.model.db.News
import io.reactivex.Completable
import io.reactivex.Flowable

interface NewsRepository {

    fun loadInitialNewsList(): Completable

    fun getNewsList(isHotNews: Boolean): Flowable<List<News>>

    fun reloadNewsList(): Completable

    fun updateNewsLocalPath(localPath: String, guid: String): Completable
}