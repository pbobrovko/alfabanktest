package com.pavel.bobrovko.alfabanktest.ui.splash

import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.pavel.bobrovko.alfabanktest.R
import com.pavel.bobrovko.alfabanktest.core.BaseFragment
import com.pavel.bobrovko.alfabanktest.model.workmanager.NewsUpdateWorker
import com.pavel.bobrovko.alfabanktest.presentation.splash.SplashPresenter
import com.pavel.bobrovko.alfabanktest.presentation.splash.SplashView
import toothpick.Scope
import java.util.concurrent.TimeUnit


class SplashFragment : BaseFragment(), SplashView {

    override val layoutRes = R.layout.fragment_splash

    @InjectPresenter
    lateinit var presenter: SplashPresenter

    @ProvidePresenter
    fun providePresenter(): SplashPresenter = scope.getInstance(SplashPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
    }

    override fun startUpdateWork() {
        val updateNewsWorkRequest = OneTimeWorkRequest.Builder(NewsUpdateWorker::class.java)
            .setInitialDelay(WORKER_DURATION, TimeUnit.MINUTES)
            .build()
        WorkManager.getInstance().enqueue(updateNewsWorkRequest)
    }

    companion object {
        private const val WORKER_DURATION: Long = 5
    }
}