package com.pavel.bobrovko.alfabanktest.presentation.newslist

import com.arellomobile.mvp.InjectViewState
import com.pavel.bobrovko.alfabanktest.R
import com.pavel.bobrovko.alfabanktest.core.BasePresenter
import com.pavel.bobrovko.alfabanktest.core.ErrorHandler
import com.pavel.bobrovko.alfabanktest.core.FlowRouter
import com.pavel.bobrovko.alfabanktest.core.Screens
import com.pavel.bobrovko.alfabanktest.core.wrappers.PrimitiveWrapper
import com.pavel.bobrovko.alfabanktest.model.interactor.NewsInteractor
import com.pavel.bobrovko.alfabanktest.model.system.resource.ResourceManager
import javax.inject.Inject

@InjectViewState
class NewsListPresenter @Inject constructor(
    private val newsInteractor: NewsInteractor,
    private val flowRouter: FlowRouter,
    private val errorHandler: ErrorHandler,
    private val isHotNews: PrimitiveWrapper<Boolean>,
    private val resourceManager: ResourceManager
) : BasePresenter<NewsListView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        val emptyMessage = if (isHotNews.value) {
            resourceManager.getString(R.string.no_hot_news)
        } else {
            resourceManager.getString(R.string.no_favorites_news)
        }
        viewState.initEmptyView(emptyMessage)

        if (!isHotNews.value) {
            viewState.disableRefresh()
        }

        newsInteractor
            .getNewsList(isHotNews.value)
            .subscribe(
                {
                    if (it.isNotEmpty()) {
                        viewState.setNewsList(it)
                        viewState.showEmptyView(false)
                    } else {
                        viewState.showEmptyView(true)
                    }
                },
                { errorHandler.handleError(it) { message -> viewState.showMessage(message) } }
            )
            .connect()
    }

    fun onNewsClicked(guid: String) {
        flowRouter.navigateTo(Screens.NewsDetailsScreen(guid, isHotNews.value))
    }

    fun refreshNewsList() {
        newsInteractor
            .reloadNewsList()
            .doAfterTerminate { viewState.setRefreshFinished() }
            .subscribe(
                {},
                {
                    errorHandler.handleError(it) { message -> viewState.showMessage(message) }
                }
            )
            .connect()
    }

    fun onBackPressed() {
        flowRouter.finishFlow()
    }
}