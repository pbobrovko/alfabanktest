package com.pavel.bobrovko.alfabanktest.model.system.resource

import android.content.Context
import javax.inject.Inject

class ResourceManagerImpl @Inject constructor(
    private val context: Context
) : ResourceManager {

    override fun getString(id: Int): String = context.getString(id)

    override fun getString(id: Int, vararg formatArgs: Any) = String.format(context.getString(id, *formatArgs))
}
