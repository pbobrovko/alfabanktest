package com.pavel.bobrovko.alfabanktest.model.repository

import com.pavel.bobrovko.alfabanktest.di.qualifier.RssServerUrl
import com.pavel.bobrovko.alfabanktest.model.db.News
import com.pavel.bobrovko.alfabanktest.model.db.NewsDao
import com.pavel.bobrovko.alfabanktest.model.system.SchedulersProvider
import com.prof.rssparser.Article
import com.prof.rssparser.OnTaskCompleted
import com.prof.rssparser.Parser
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val rssParser: Parser,
    private val newsDatabase: NewsDao,
    @RssServerUrl private val apiUrl: String,
    private val schedulersProvider: SchedulersProvider
) : NewsRepository {

    override fun loadInitialNewsList() =
        newsDatabase.getHot()
            .take(1)
            .flatMapCompletable {
                if (it.isEmpty()) {
                    parseRss()
                        .flatMapCompletable {
                            newsDatabase.insert(it)
                        }
                } else {
                    Completable.complete()
                }
            }
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())

    override fun getNewsList(isHotNews: Boolean): Flowable<List<News>> =
        Flowable
            .defer {
                if (isHotNews) {
                    newsDatabase.getHot()
                } else {
                    newsDatabase.getFavorites()
                }
            }
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())

    override fun reloadNewsList() =
        parseRss()
            .flatMapCompletable {
                newsDatabase.insert(it)
            }

    override fun updateNewsLocalPath(localPath: String, guid: String) =
        newsDatabase.updateLocalPath(localPath, guid)
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())

    private fun parseRss() =
        Single.create<List<News>> {
            rssParser.onFinish(object : OnTaskCompleted {

                override fun onTaskCompleted(list: MutableList<Article>) {
                    if (!it.isDisposed) {
                        it.onSuccess(list.map { mapNews(it) })
                    }
                }

                override fun onError(e: Exception) {
                    if (!it.isDisposed) {
                        it.onError(e)
                    }
                }
            })
            rssParser.execute(apiUrl)
        }

    private fun mapNews(article: Article): News {
        val pubDate = article.pubDate?.let {
            ZonedDateTime.parse(it, DateTimeFormatter.RFC_1123_DATE_TIME)
        }

        return News(
            title = article.title ?: "",
            link = article.link ?: "",
            description = article.description ?: "",
            pubDate = pubDate,
            guid = article.guid ?: "",
            localPath = ""
        )
    }
}