package com.pavel.bobrovko.alfabanktest.extension

import android.content.Context
import android.net.ConnectivityManager
import android.util.DisplayMetrics
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Replace
import java.io.File


fun Navigator.setLaunchScreen(screen: SupportAppScreen) {

    applyCommands(
        arrayOf(
            BackTo(null),
            Replace(screen)
        )
    )
}

fun Context.isInternetAvailable(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val netInfo = connectivityManager.activeNetworkInfo
    return netInfo != null && netInfo.isConnected
}

fun Context.createPagesDir() {
    val dir = File(getPagesPath())
    if (!dir.exists()) {
        dir.mkdir()
    }
}

fun Context.getPagesPath() = "${filesDir.absolutePath}/pages/"

fun Context.dpToPx(dp: Float) =
    Math.round(dp * (resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT))