package com.pavel.bobrovko.alfabanktest.ui.newslist.adapter

import android.view.View
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.pavel.bobrovko.alfabanktest.R
import com.pavel.bobrovko.alfabanktest.model.db.News
import org.threeten.bp.format.DateTimeFormatter

class NewsListViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    private val itemTitle = itemView.findViewById<TextView>(R.id.itemTitleText)
    private val itemDate = itemView.findViewById<TextView>(R.id.itemDateText)
    private val itemDescription = itemView.findViewById<TextView>(R.id.itemDescriptionText)

    fun bindView(item: News) {
        itemTitle.text = item.title
        itemDescription.text = HtmlCompat.fromHtml(item.description, HtmlCompat.FROM_HTML_MODE_LEGACY)
        if (item.pubDate != null) {
            itemDate.text = DateTimeFormatter.ofPattern(DATE_PATTERN).format(item.pubDate)
            itemDate.visibility = View.VISIBLE
        } else {
            itemDate.visibility = View.GONE
        }
    }

    companion object {
        private const val DATE_PATTERN = "dd.MM.YYYY HH:mm"
    }
}