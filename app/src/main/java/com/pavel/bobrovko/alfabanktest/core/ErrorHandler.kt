package com.pavel.bobrovko.alfabanktest.core

import com.pavel.bobrovko.alfabanktest.R
import com.pavel.bobrovko.alfabanktest.model.system.resource.ResourceManager
import java.net.UnknownHostException
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class ErrorHandler @Inject constructor(
    private val resourceManager: ResourceManager
) {

    fun handleError(exception: Throwable, messageListener: (String) -> Unit = {}) {
        when (exception) {
            is TimeoutException -> {
                messageListener(resourceManager.getString(R.string.timeout_error))
            }
            is UnknownHostException,
            is ExecutionException -> {
                messageListener(resourceManager.getString(R.string.network_error))
            }
            else -> {
                messageListener(resourceManager.getString(R.string.unknown_error))
            }
        }
    }
}