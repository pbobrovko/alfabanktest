package com.pavel.bobrovko.alfabanktest.ui.newslist.adapter

import androidx.recyclerview.widget.DiffUtil
import com.pavel.bobrovko.alfabanktest.model.db.News

class NewsListDiffCallback(
    private val oldList: List<News>,
    private val newList: List<News>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].guid == newList[newItemPosition].guid
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}