package com.pavel.bobrovko.alfabanktest.ui.newsdetails

import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.snackbar.Snackbar
import com.pavel.bobrovko.alfabanktest.R
import com.pavel.bobrovko.alfabanktest.core.BaseFragment
import com.pavel.bobrovko.alfabanktest.core.wrappers.PrimitiveWrapper
import com.pavel.bobrovko.alfabanktest.extension.createPagesDir
import com.pavel.bobrovko.alfabanktest.model.db.News
import com.pavel.bobrovko.alfabanktest.presentation.newsdetails.NewsDetailsPresenter
import com.pavel.bobrovko.alfabanktest.presentation.newsdetails.NewsDetailsView
import com.pavel.bobrovko.alfabanktest.ui.newsdetails.viewPager.NewsViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_news_details.*
import toothpick.Scope
import toothpick.config.Module

class NewsDetailsFragment : BaseFragment(), NewsDetailsView, OnNewsArchiveListener {

    override val layoutRes = R.layout.fragment_news_details

    @InjectPresenter
    lateinit var presenter: NewsDetailsPresenter

    @ProvidePresenter
    fun providePresenter(): NewsDetailsPresenter = scope.getInstance(NewsDetailsPresenter::class.java)

    private lateinit var pagerAdapter: NewsViewPagerAdapter
    private var snackbar: Snackbar? = null
    private var selectedPage = NO_POSITION

    override fun onCreate(savedInstanceState: Bundle?) {
        if (arguments == null) {
            throw IllegalArgumentException("${javaClass.canonicalName} need arguments")
        }
        super.onCreate(savedInstanceState)

        savedInstanceState?.let {
            selectedPage = it.getInt(KEY_SELECTED_PAGE)
        }
    }

    override fun installScopeModules(scope: Scope) {
        scope.installModules(object : Module() {
            init {
                bind(String::class.java).toInstance(arguments!!.getString(INITIAL_GUID))
                bind(PrimitiveWrapper::class.java)
                    .toInstance(PrimitiveWrapper(arguments!!.getBoolean(IS_HOT_NEWS)))
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pagerAdapter = NewsViewPagerAdapter(childFragmentManager)
        newsViewPager.offscreenPageLimit = 3
        newsViewPager.adapter = pagerAdapter
        newsViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                selectedPage = position
            }

        })

        newsDetailsToolbar.setNavigationOnClickListener { onBackPressed() }
    }

    override fun createPagesDir() {
        requireContext().createPagesDir()
    }

    override fun setNewsList(newsList: List<News>) {
        pagerAdapter.setItems(newsList)
    }

    override fun setInitialPosition(newsPosition: Int) {
        if (selectedPage == NO_POSITION) {
            selectedPage = newsPosition
        }
        newsViewPager.setCurrentItem(selectedPage, false)
    }

    override fun showMessage(message: String) {
        snackbar?.dismiss()
        snackbar = Snackbar.make(newsDetailsLayout, message, Snackbar.LENGTH_LONG)
        snackbar?.show()
    }

    override fun setProgressVisible() {
        loadNewsProgressBar.visibility = View.VISIBLE
    }

    override fun setProgressGone() {
        loadNewsProgressBar.visibility = View.GONE
    }

    override fun onNewsArchived(localPath: String, guid: String) {
        presenter.onNewsArchived(localPath, guid)
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(KEY_SELECTED_PAGE, selectedPage)
    }

    companion object {
        private const val INITIAL_GUID = "initialGuid"
        private const val IS_HOT_NEWS = "isHotNews"
        private const val KEY_SELECTED_PAGE = "keySelectedPage"

        private const val NO_POSITION = -1

        fun newInstance(guid: String, isHotNews: Boolean) =
            NewsDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(INITIAL_GUID, guid)
                    putBoolean(IS_HOT_NEWS, isHotNews)
                }
            }
    }
}