package com.pavel.bobrovko.alfabanktest.model.system.resource

interface ResourceManager {

    fun getString(id: Int): String

    fun getString(id: Int, vararg formatArgs: Any): String
}