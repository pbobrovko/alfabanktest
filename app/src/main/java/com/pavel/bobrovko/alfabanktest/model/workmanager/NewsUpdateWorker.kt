package com.pavel.bobrovko.alfabanktest.model.workmanager

import android.content.Context
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.pavel.bobrovko.alfabanktest.model.interactor.NewsInteractor
import io.reactivex.Single

class NewsUpdateWorker(
    context: Context,
    params: WorkerParameters,
    private val newsInteractor: NewsInteractor
) : RxWorker(context, params) {

    override fun createWork(): Single<Result> {
        return newsInteractor
            .reloadNewsList()
            .toSingleDefault(Result.retry())
            .onErrorReturnItem(Result.retry())
    }
}