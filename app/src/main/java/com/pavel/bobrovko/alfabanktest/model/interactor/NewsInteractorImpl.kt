package com.pavel.bobrovko.alfabanktest.model.interactor

import com.pavel.bobrovko.alfabanktest.model.db.News
import com.pavel.bobrovko.alfabanktest.model.repository.NewsRepository
import io.reactivex.Flowable
import javax.inject.Inject

class NewsInteractorImpl @Inject constructor(
    private val newsRepository: NewsRepository
) : NewsInteractor {

    override fun loadInitialNewsList() = newsRepository.loadInitialNewsList()

    override fun getNewsList(isHotNews: Boolean): Flowable<List<News>> = newsRepository.getNewsList(isHotNews)

    override fun reloadNewsList() = newsRepository.reloadNewsList()

    override fun updateNewsLocalPath(localPath: String, guid: String) =
        newsRepository.updateNewsLocalPath(localPath, guid)
}