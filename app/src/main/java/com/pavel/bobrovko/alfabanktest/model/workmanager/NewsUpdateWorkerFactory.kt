package com.pavel.bobrovko.alfabanktest.model.workmanager

import android.content.Context
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import com.pavel.bobrovko.alfabanktest.model.interactor.NewsInteractor
import javax.inject.Inject

class NewsUpdateWorkerFactory @Inject constructor(
    private val newsInteractor: NewsInteractor
) : WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ) = NewsUpdateWorker(appContext, workerParameters, newsInteractor)
}