package com.pavel.bobrovko.alfabanktest.presentation.newslist

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.pavel.bobrovko.alfabanktest.model.db.News

@StateStrategyType(AddToEndSingleStrategy::class)
interface NewsListView : MvpView {

    fun setNewsList(newsList: List<News>)

    fun setRefreshFinished()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    fun initEmptyView(message: String)

    fun showEmptyView(show: Boolean)

    fun disableRefresh()
}