package com.pavel.bobrovko.alfabanktest.core

import com.pavel.bobrovko.alfabanktest.ui.main.MainFlowFragment
import com.pavel.bobrovko.alfabanktest.ui.newsdetails.NewsDetailsFragment
import com.pavel.bobrovko.alfabanktest.ui.newslist.NewsListContainerFragment
import com.pavel.bobrovko.alfabanktest.ui.splash.SplashFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    object MainFlow : SupportAppScreen() {
        override fun getFragment() = MainFlowFragment()
    }

    object SplashScreen : SupportAppScreen() {
        override fun getFragment() = SplashFragment()
    }

    object NewsListContainerScreen : SupportAppScreen() {
        override fun getFragment() = NewsListContainerFragment()
    }

    data class NewsDetailsScreen(val guid: String, val isHotNews: Boolean) : SupportAppScreen() {
        override fun getFragment() = NewsDetailsFragment.newInstance(guid, isHotNews)
    }
}